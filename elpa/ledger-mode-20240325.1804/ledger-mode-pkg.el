(define-package "ledger-mode" "20240325.1804" "Helper code for use with the \"ledger\" command-line tool"
  '((emacs "25.1"))
  :commit "88a89b91265de26ccf48e1f378bd021d91adb83d")
;; Local Variables:
;; no-byte-compile: t
;; End:
