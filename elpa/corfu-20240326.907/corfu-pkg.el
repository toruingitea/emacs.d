(define-package "corfu" "20240326.907" "COmpletion in Region FUnction"
  '((emacs "27.1")
    (compat "29.1.4.4"))
  :commit "afc889593494dce75c1a4727f6f0d860a59de576" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("abbrev" "convenience" "matching" "completion" "text")
  :url "https://github.com/minad/corfu")
;; Local Variables:
;; no-byte-compile: t
;; End:
