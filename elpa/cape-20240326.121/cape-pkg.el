(define-package "cape" "20240326.121" "Completion At Point Extensions"
  '((emacs "27.1")
    (compat "29.1.4.4"))
  :commit "3b1ade4b2833856ee969478e4fea6846a893cdfa" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("abbrev" "convenience" "matching" "completion" "text")
  :url "https://github.com/minad/cape")
;; Local Variables:
;; no-byte-compile: t
;; End:
