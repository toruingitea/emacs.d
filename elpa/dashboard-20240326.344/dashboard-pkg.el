(define-package "dashboard" "20240326.344" "A startup screen extracted from Spacemacs"
  '((emacs "26.1"))
  :commit "e012e2aaf20f2a73a7b9f23d9cdb893d696e7549" :authors
  '(("Rakan Al-Hneiti" . "rakan.alhneiti@gmail.com"))
  :maintainers
  '(("Jesús Martínez" . "jesusmartinez93@gmail.com"))
  :maintainer
  '("Jesús Martínez" . "jesusmartinez93@gmail.com")
  :keywords
  '("startup" "screen" "tools" "dashboard")
  :url "https://github.com/emacs-dashboard/emacs-dashboard")
;; Local Variables:
;; no-byte-compile: t
;; End:
