(add-to-list 'load-path "~/.emacs.d/config/")

(require 'buffer-move)          ;; Buffer-move for better window management
;(require 'elpaca-setup)         ;; The Elpaca Package Manager

(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

  ;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(setq default-directory "~/")

; Tree-sitter
(require 'treesit)

;; Setting RETURN key in org-mode to follow links
(setq org-return-follows-link  t)

(setq user-full-name "toru"
      user-mail-address "toru777average@cock.li")

;(global-undo-tree-mode)

;; Save last place in file
(save-place-mode 1)

;; Enable mouse-support for the terminal-version
(xterm-mouse-mode 1)

(setq display-buffer-alist
 '(
   ("\\*Occur\\*"
    (display-buffer-reuse-mode-window
     display-buffer-below-selected)
     (window-height . fit-window-to-buffer)
   )))

;; Window undo/redo commands
;(winner-mode 1)

(pixel-scroll-precision-mode 1)

(defvar +scroll-delta 180)

(defun +scroll-up-some ()
  (interactive)
  (pixel-scroll-precision-scroll-up +scroll-delta))

(defun +scroll-down-some ()
  (interactive)
  (pixel-scroll-precision-scroll-down +scroll-delta))

 (defun +bind-scroll-keys (mode-map)
   (evil-define-key '(motion normal) mode-map (kbd "K") '+scroll-up-some)
   (evil-define-key '(motion normal) mode-map (kbd "J") '+scroll-down-some))

;; no backup files with ~ at the end
(setq make-backup-files nil)

;; Temp/backups files directory
(setq backup-directory-alist '((".*" . "~/.local/share/Trash/files")))

;; Italic comments
(set-face-attribute 'default nil :height 120)
(set-face-attribute 'font-lock-comment-face nil
                    :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
                    :slant 'italic)
(setq-default line-spacing 0.12)

;; General Settings
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(setq org-hide-emphasis-markers t)
(setq native-comp-async-report-warnings-errors 'silent)
(setq org-edit-src-content-indentation 0)
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
(setq confirm-kill-emacs nil)
(eval-after-load 'auto-complete '(global-auto-complete-mode 1))
(global-set-key [escape] 'keyboard-escape-quit)

(setq use-file-dialog nil)  ;; No file dialog
(setq use-dialog-box nil)   ;; No dialog box
(setq pop-up-window nil)    ;; No popup windows
(delete-selection-mode 1)    ;; You can select text and delete it by typing.
(electric-indent-mode 1)    ;; Turn on the weird indenting that Emacs does by default.
(electric-pair-mode 1)       ;; Turns on automatic parens pairing
;; The following prevents <> from auto-pairing when electric-pair-mode is on.
;; Otherwise, org-tempo is broken when you try to <s TAB...
(add-hook 'org-mode-hook (lambda ()
                           (setq-local electric-pair-inhibit-predicate
                                       `(lambda (c)
                                          (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))
(global-auto-revert-mode t)  ;; Automatically show changes if the file has changed
(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)
(global-visual-line-mode t)  ;; Enable truncated lines
(menu-bar-mode -1)           ;; Disable the menu bar
(scroll-bar-mode -1)         ;; Disable the scroll bar
(tool-bar-mode -1)           ;; Disable the tool bar
(setq tool-bar-style 'both)
(setq org-edit-src-content-indentation 0) ;; Set src block automatic indent to 0 instead of 2.
(setq redisplay-dont-pause t
      scroll-margin 5
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1)

(setq inhibit-startup-message t) ;doesnt show the default emacs startpage
(setq inhibit-startup-screen t)
;(setq initial-buffer-choice 'about-emacs)
;(switch-to-buffer-other-window "*scratch*")

(use-package dashboard
  :ensure t
  :init
  (setq initial-buffer-choice 'dashboard-open)
  (setq dashboard-set-footer nil)

  (setq dashboard-icon-type 'all-the-icons) ;; use `all-the-icons' package
  (setq dashboard-display-icons-p t) ;; display icons on both GUI and terminal
  (setq dashboard-icon-type 'nerd-icons) ;; use `nerd-icons' package
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)

  (setq dashboard-set-init-info t)
   ;(setq dashboard-init-info "This is an init message!")
   (setq dashboard-show-shortcuts t)
   (setq dashboard-banner-logo-title "More Than a Text Editor")
   (setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  (setq dashboard-startup-banner "~/.emacs.d/img/dr.png")  ;; use custom image as banner
  (setq dashboard-page-separator "\n\^L\n")
  (setq dashboard-center-content t) ;; set to 't' for centered content
  (setq dashboard-items '((recents  . 5)
                       (bookmarks . 5)))
:config
  (dashboard-setup-startup-hook))

;; Theme
(load-theme 'ef-trio-dark t)

(setq modus-themes-org-blocks 'gray-background)
(add-to-list 'custom-theme-load-path "~/.emacs.d/etc/themes")

;; Transparency
(set-frame-parameter nil 'alpha-background 100)
(add-to-list 'default-frame-alist '(alpha-background . 100))

; Font
(set-face-attribute 'default nil
		    :family "jetbrains mono"
		    :height 133
		    ;;:weight 'semilight
		    )

;; Blinking cursor
(setq blink-cursor-mode nil)

(global-prettify-symbols-mode t)

;; Buffers
;(global-tab-line-mode 1)

;; Tabs
;(tab-bar-mode 1)

;; (setq-default prettify-symbols-alist '(("#+BEGIN_SRC" . "†")
;;                                        ("#+END_SRC" . "†")
;;                                        ("#+begin_src" . "†")
;;                                        ("#+end_src" . "†")
;;                                        (">=" . "≥")
;;                                        ("=>" . "⇨")))
;; (setq prettify-symbols-unprettify-at-point 'right-edge)
;; (add-hook 'org-mode-hook 'prettify-symbols-mode)

(use-package simple-modeline
  :hook (after-init . simple-modeline-mode))

;; Default custom modeline
(setq-default mode-line-format (delq 'mode-line-modes mode-line-format))

'(mode-line ((t (:background "color-233" :foreground "cyan"))))
 '(mode-line-inactive ((t (:inherit mode-line :background "color-233" :foreground "brightblack" :weight light))))

;; DRAW A BOX AROUND THE MODELINE
;; (set-face-attribute 'mode-line nil
;;                 :box '(:line-width 1 :color "white"))

(setq display-time-day-and-date t
      display-time-format "%a, %d-%m-%y %I:%M") ;; displays date

(display-time-mode 1) ;; displays current time

(setq display-time-default-load-average nil)
(setq display-time-load-average nil)

(use-package general
  :ensure t
  :config
  (general-evil-setup)
  (eval-after-load "org" '(define-key org-mode-map (kbd "C-j") nil))
  (eval-after-load "org" '(define-key org-mode-map (kbd "C-k") nil))
  (eval-after-load "org" '(define-key org-mode-map (kbd "M-l") nil))
  (general-define-key
   :states '(normal insert motion)
   "C-h" 'evil-window-left
   "C-j" 'evil-window-down
   "C-k" 'evil-window-up
   "C-l" 'evil-window-right
   "M-l" 'org-make-olist)

  (general-create-definer user/leader-keys
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC" ;; set 'SPC' as leader key
    :global-prefix "C-SPC") ;; access leader in insert mode
  
  (user/leader-keys
    "." '(find-file :wk "Find file")
    ;"=" '(perspective-map :wk "Perspective") ;; Lists all the perspective keybindings
    "TAB TAB" '(comment-line :wk "Comment lines")
    "u" '(universal-argument :wk "Universal argument"))

   (user/leader-keys
    "a" '(:ignore t :wk "Agenda buffers")
    "a" '(org-agenda :wk "Open the agenda"))

  (user/leader-keys
    "b" '(:ignore t :wk "Bookmarks/Buffers")
    "b r" '(recentf :wk "Recent files")
    "b h" '(hydra-navigation/body :wk "Hydra")
    "b c" '(clone-indirect-buffer :wk "Create indirect buffer copy in a split")
    "b C" '(clone-indirect-buffer-other-window :wk "Clone indirect buffer in new window")
    "b d" '(bookmark-delete :wk "Delete bookmark")
    "b g" '(consult-buffer :wk "Consult global")
    "b i" '(switch-to-buffer :wk "Switch to buffer")
    "b k" '(kill-current-buffer :wk "Kill current buffer")
    "b K" '(kill-some-buffers :wk "Kill multiple buffers")
    "b l" '(consult-bookmark :wk "List bookmarks")
    "b m" '(bookmark-set :wk "Set bookmark")
    "b n" '(next-buffer :wk "Next buffer")
    "b p" '(previous-buffer :wk "Previous buffer")
    "b x" '(revert-buffer :wk "Reload buffer")
    "b R" '(rename-buffer :wk "Rename buffer")
    "b s" '(basic-save-buffer :wk "Save buffer")
    "b S" '(save-some-buffers :wk "Save multiple buffers")
    "b w" '(bookmark-save :wk "Save current bookmarks to bookmark file"))

  (user/leader-keys
    "d" '(:ignore t :wk "Dired")
    "d d" '(dired :wk "Open dired")
    "d f" '(wdired-finish-edit :wk "Writable dired finish edit"))

  (user/leader-keys
    "e" '(:ignore t :wk "Eshell/Eval/EWW")    
    "e b" '(eval-buffer :wk "Evaluate elisp in buffer")
    "e d" '(eval-defun :wk "Evaluate defun containing or after point")
    "e e" '(eval-expression :wk "Evaluate and elisp expression")
    "e l" '(eval-last-sexp :wk "Evaluate elisp expression before point")
    "e r" '(eval-region :wk "Evaluate elisp in region")
    "e R" '(eww-reload :which-key "Reload current page in EWW")
    "e s" '(eshell :which-key "Eshell")
    "e w" '(eww :which-key "EWW emacs web wowser"))
  
  (user/leader-keys
    "i" '(:ignore t :wk "Insert")
    "i i" '(ispell :wk "Insert ispell")
    "i d" '(org-id-get-create :wk "Insert ID")
    "i n" '(increment-number-at-point :wk "Increment numbers")
    "i m" '(my-increment-number-decimal :wk "Increment numbers advanced")
    "i c" '(org-capture :wk "Capture")
    "i s" '(yas-insert-snippet :wk "Insert snippet"))

  (user/leader-keys
    "p" '(:ignore t :wk "Projects")
    "p p" '(project-switch-project :wk "Search projects")
    "p d" '(project-find-dir :wk "Find directory project"))

  (user/leader-keys
    "r" '(:ignore t :wk "Org-Roam")
    "r b" '(org-roam-buffer-toggle :wk "Toggle buffer")
    "r f" '(org-roam-node-find :wk "Show nodes")
    "r i" '(org-roam-node-insert :wk "Insert node link"))

  (user/leader-keys
    "s" '(:ignore t :wk "Search")
    "s o" '(occur :wk "Occur")
    ;"s u" '(undo-tree-visualize :wk "Undo-tree")
    "s l" '(consult-outline :wk "Travel on headings"))

    (user/leader-keys
    "h" '(:ignore t :wk "Help")
    "h b" '(describe-bindings :wk "Describe bindings")
    "h c" '(describe-char :wk "Describe character under cursor")
    "h d" '(:ignore t :wk "Emacs documentation")
    "h d a" '(about-emacs :wk "About Emacs")
    "h d d" '(view-emacs-debugging :wk "View Emacs debugging")
    "h d f" '(view-emacs-FAQ :wk "View Emacs FAQ")
    "h d m" '(info-emacs-manual :wk "The Emacs manual")
    "h d n" '(view-emacs-news :wk "View Emacs news")
    "h d o" '(describe-distribution :wk "How to obtain Emacs")
    "h d p" '(view-emacs-problems :wk "View Emacs problems")
    "h d t" '(view-emacs-todo :wk "View Emacs todo")
    "h d w" '(describe-no-warranty :wk "Describe no warranty")
    "h e" '(view-echo-area-messages :wk "View echo area messages")
    "h f" '(describe-function :wk "Describe function")
    "h F" '(describe-face :wk "Describe face")
    "h g" '(describe-gnu-project :wk "Describe GNU Project")
    "h i" '(info :wk "Info")
    "h I" '(describe-input-method :wk "Describe input method")
    "h k" '(describe-key :wk "Describe key")
    "h l" '(view-lossage :wk "Display recent keystrokes and the commands run")
    "h L" '(describe-language-environment :wk "Describe language environment")
    "h m" '(describe-mode :wk "Describe mode")
    "h r" '(:ignore t :wk "Reload")
    "h r r" '((lambda () (interactive)
                (load-file "~/.emacs.d/init.el"))
              :wk "Reload emacs config")
    "h t" '(consult-theme :wk "Load theme")
    "h v" '(describe-variable :wk "Describe variable")
    "h w" '(where-is :wk "Prints keybinding for command if set")
    "h x" '(describe-command :wk "Display full documentation for command"))

  (user/leader-keys
    "v" '(:ignore t :wk "Vc")
    "v d" '(vc-diff :wk "Show diff")
    "v f" '(vc-dir :wk "Choose vc directories")
    "v p" '(vc-push :wk "Push")
    "v n" '(vc-next-action :wk "Vc next action"))

  (user/leader-keys
    "t" '(:ignore t :wk "Toggle")
    "t c" '(comment-line :wk "Toggle comment lines")
    "t o" '(olivetti-mode :wk "Toggle olivetti-mode")
    "t n" '(display-line-numbers-mode :wk "Toggle line-numbers")
    "t e" '(embark-act :wk "Embark")
    "t d" '(dired-sidebar-toggle-sidebar :wk "Toggle dired-sidebar")
    "t l" '(org-make-list :wk "Make automatic numerical lists"))

 (user/leader-keys
    "w" '(:ignore t :wk "Windows")
    ;; Window splits
    "w c" '(evil-window-delete :wk "Close window")
    "w n" '(evil-window-new :wk "New window")
    "w s" '(evil-window-split :wk "Horizontal split window")
    "w v" '(evil-window-vsplit :wk "Vertical split window")

    ;; Window motions
    "w h" '(evil-window-left :wk "Window left")
    "w j" '(evil-window-down :wk "Window down")
    "w k" '(evil-window-up :wk "Window up")
    "w l" '(evil-window-right :wk "Window right")
    "w w" '(evil-window-next :wk "Goto next window")

    ;; Move Windows
    "w H" '(buf-move-left :wk "Buffer move left")
    "w J" '(buf-move-down :wk "Buffer move down")
    "w K" '(buf-move-up :wk "Buffer move up")
    "w L" '(buf-move-right :wk "Buffer move right"))

)

(require 'bind-key)
(bind-key* "<C-return>" 'toru/insert-item-below)
(global-set-key (kbd "C-s") 'consult-line)
(global-set-key (kbd "C-x k") 'image-kill-buffer)
(global-set-key (kbd "C-x c") 'calendar)
(global-set-key (kbd "C-x C-b") 'ibuffer)
;(global-set-key (kbd "C-x <right>") 'centaur-tabs-forward)
;(global-set-key (kbd "C-x <left>") 'centaur-tabs-backward)
;(global-set-key (kbd "C-v") 'consult-yank-pop)
;; (global-set-key (kbd "C-c <right>") 'tab-line-switch-to-next-tab)
;; (global-set-key (kbd "C-c <left>") 'tab-line-switch-to-prev-tab)
;;;(global-set-key (kbd "C-v") 'org-yank)
;; (global-set-key (kbd "C-z") 'undo-tree-undo)
;; (global-set-key (kbd "C-S-z") 'undo-tree-redo)
;; (global-set-key (kbd "C-<tab>") 'universal-argument)
;; (global-set-key (kbd "C-q") 'kill-ring-save)

(global-set-key (kbd "M-a") 'other-window)

;; (global-set-key (kbd "M-s l") 'consult-outline)
;; (global-set-key (kbd "M-s o") 'occur)
;; (global-set-key (kbd "M-y") 'scroll-up-command)

(setq scroll-preserve-screen-position 1)

;;scroll window up/down by one line
(global-set-key (kbd "M-n") (kbd "C-u 1 C-v"))
(global-set-key (kbd "M-p") (kbd "C-u 1 M-v"))

(defun export-org-to-pdf-and-cleanup ()
  "Export current org file to PDF, delete generated .log and .tex files, and move PDF to a specific folder."
  (interactive)
  (let* ((org-file (buffer-file-name)) 
         (pdf-folder "~/dc/craft-tools/mispdfs/") 
         (pdf-file (concat pdf-folder (file-name-base org-file) ".pdf")) 
         (default-directory (file-name-directory org-file))) ; Set default directory for export
    (org-latex-export-to-pdf) 
    (delete-file (concat (file-name-base org-file) ".log")) 
    (delete-file (concat (file-name-base org-file) ".tex"))  
    (rename-file (concat (file-name-base org-file) ".pdf") pdf-file t) 
    (message "Exported org file to PDF and cleaned up.")))

(global-set-key (kbd "C-c e") 'export-org-to-pdf-and-cleanup)

    (defun increment-number-at-point ()
      (interactive)
      (skip-chars-backward "0-9")
      (or (looking-at "[0-9]+")
          (error "No number at point"))
      (replace-match (number-to-string (1+ (string-to-number (match-string 0))))))

;; Disable line-numbers on fireplace-mode
(add-hook 'fireplace-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; Disable line-numbers on org-agenda
(add-hook 'org-agenda-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; Disable line-numbers on pdf-view-mode
(add-hook 'pdf-view-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; Disable line-numbers on term
(add-hook 'term-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; Disable line-numbers on doc-view-mode
(add-hook 'doc-view-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; Start GNUS on Emacs startup
;; (add-hook 'emacs-startup-hook
;;           (lambda ()
;;             (gnus)))

;; Create a list selecting several lines
(defun org-make-list (arg)
  (interactive "P")
  (let ((n (or arg 1)))
    (when (region-active-p)
      (setq n (count-lines (region-beginning)
                           (region-end)))
      (goto-char (region-beginning)))
    (dotimes (i n)
      (beginning-of-line)
      (insert (concat (number-to-string (1+ i)) ". "))
      (forward-line))))

;; Create list with C-Enter
(defun toru--insert-item (direction)
  (let ((context (org-element-lineage
                  (org-element-context)
                  '(table table-row headline inlinetask item plain-list)
                  t)))
    (pcase (org-element-type context)
      ;; Add a new list item (carrying over checkboxes if necessary)
      ((or `item `plain-list)
       (let ((orig-point (point)))
         ;; Position determines where org-insert-todo-heading and `org-insert-item'
         ;; insert the new list item.
         (if (eq direction 'above)
             (org-beginning-of-item)
           (end-of-line))
         (let* ((ctx-item? (eq 'item (org-element-type context)))
                (ctx-cb (org-element-property :contents-begin context))
                ;; Hack to handle edge case where the point is at the
                ;; beginning of the first item
                (beginning-of-list? (and (not ctx-item?)
                                         (= ctx-cb orig-point)))
                (item-context (if beginning-of-list?
                                  (org-element-context)
                                context))
                ;; Horrible hack to handle edge case where the
                ;; line of the bullet is empty
                (ictx-cb (org-element-property :contents-begin item-context))
                (empty? (and (eq direction 'below)
                             ;; in case contents-begin is nil, or contents-begin
                             ;; equals the position end of the line, the item is
                             ;; empty
                             (or (not ictx-cb)
                                 (= ictx-cb
                                    (1+ (point))))))
                (pre-insert-point (point)))
           ;; Insert dummy content, so that `org-insert-item'
           ;; inserts content below this item
           (when empty?
             (insert " "))
           (org-insert-item (org-element-property :checkbox context))
           ;; Remove dummy content
           (when empty?
             (delete-region pre-insert-point (1+ pre-insert-point))))))
      ;; Add a new table row
      ((or `table `table-row)
       (pcase direction
         ('below (save-excursion (org-table-insert-row t))
                 (org-table-next-row))
         ('above (save-excursion (org-shiftmetadown))
                 (toru/table-previous-row))))

      ;; Otherwise, add a new heading, carrying over any todo state, if
      ;; necessary.
      (_
       (let ((level (or (org-current-level) 1)))
         ;; I intentionally avoid `org-insert-heading' and the like because they
         ;; impose unpredictable whitespace rules depending on the cursor
         ;; position. It's simpler to express this command's responsibility at a
         ;; lower level than work around all the quirks in org's API.
         (pcase direction
           (`below
            (let (org-insert-heading-respect-content)
              (goto-char (line-end-position))
              (org-end-of-subtree)
              (insert "\n" (make-string level ?*) " ")))
           (`above
            (org-back-to-heading)
            (insert (make-string level ?*) " ")
            (save-excursion (insert "\n"))))
         (run-hooks 'org-insert-heading-hook)
         (when-let* ((todo-keyword (org-element-property :todo-keyword context))
                     (todo-type    (org-element-property :todo-type context)))
           (org-todo
            (cond ((eq todo-type 'done)
                   ;; Doesn't make sense to create more "DONE" headings
                   (car (toru-get-todo-keywords-for todo-keyword)))
                  (todo-keyword)
                  ('todo)))))))

    (when (org-invisible-p)
      (org-show-hidden-entry))
    (when (and (bound-and-true-p evil-local-mode)
               (not (evil-emacs-state-p)))
      (evil-insert 1))))

(defun toru/insert-item-below (count)
  "Inserts a new heading, table cell or item below the current one."
  (interactive "p")
  (dotimes (_ count) (toru--insert-item 'below)))

(defalias 'lp 'org-latex-export-to-pdf)
(defalias 'cf 'consult-find)
(defalias 'cr 'consult-ripgrep)
(defalias 'rc 'recentf-cleanup)
(defalias 'db 'org-roam-db-sync)
(defalias 'id 'org-roam-update-org-id-locations)

;; Org-directory
(setq org-directory "/media/tomb/org/")

(require 'org-id)

(setq org-ellipsis "⤵")

;; Scratch buffer default > org-mode
(setq initial-major-mode 'org-mode)

;; Links org files with their IDs, not their file names
(setq org-id-link-to-org-use-id t)

;; Default mode
(setq-default major-mode 'org-mode)

;; Scratch buffer default message
; if you want a message, change the balue 'nil' with "YOUR MESSAGE"
(setq initial-scratch-message nil)

;; Table of contents package
(use-package toc-org
  :ensure t
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

;; Tags list
;; (setq org-tag-alist '(("@portada" . ?p) ("@hacer" . ?h) ("incompleto" . ?i)))

(add-hook 'org-mode-hook 'org-indent-mode)

(eval-after-load 'org-indent '(diminish 'org-indent-mode))

;; Tamaño de headers (titulos)
(custom-set-faces
 '(org-level-1 ((t (:inherit outline-1 :height 1.3))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.2))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
 '(org-level-6 ((t (:inherit outline-5 :height 1.0))))
 '(org-level-7 ((t (:inherit outline-5 :height 1.0)))))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch)

(setq org-agenda-files '("/media/tomb/org/agenda/todos.org"
                           "/media/tomb/org/agenda/plans.org"
                           ;"/media/tomb/org/agenda/habit.org"
                           "/media/tomb/org/agenda/reviews.org"))
(setq org-log-done 'time) ;;put a timestamp when a TODO is done
(setq org-agenda-start-with-log-mode t)
(setq org-log-into-drawer t)
(define-key global-map (kbd "C-c a") #'org-agenda)

(setq org-agenda-window-setup 'switch-to-buffer-other-window)
(setq org-agenda-block-separator t)
(setq org-agenda-span 'day) ;; default agenda view
;(run-with-idle-timer 300 t (lambda () (org-agenda nil "a")) ) ;; regenerate agenda

;; TRUE REGENERATION
(setq org-todo-keywords
      '((sequence "TODO(t)" "PLAN(p)" "REVIEW(v)" "|" "CANCELLED(c!)" "DONE(d!)")))

;; (require 'org-habit)
;; (add-to-list 'org-modules 'org-habit)
;; (setq org-habit-graph-column 60)
;; (setq org-habit-show-habits-only-for-today t)

;; Custom agenda
(setq org-agenda-custom-commands
      '(("d" . "Toru Agenda")
        ("dd" "Unscheduled TODO"
         ((todo ""
                ((org-agenda-overriding-header "\nUnscheduled TODO")
                 (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp)))))
         nil
         nil)))

;; BASE
(use-package org-roam
:ensure t
:custom
(org-roam-directory (file-truename "/media/tomb/org/"))
:config

;; If you're using a vertical completion framework, you might want a more informative completion interface
(setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
(org-roam-db-autosync-mode t))

;; DAILIES
(setq org-roam-dailies-directory "/media/tomb/org/daily/")
(setq org-roam-dailies-capture-templates
      '(("j" "journal entry" entry
         "* %<%r>: %?"
         :target (file+head "%<%Y-%m-%d>.org"
                            "#+title: %<%Y-%m-%d>\n"))))

(setq org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)))

;; UI
(use-package org-roam-ui
    :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

;(setq org-default-notes-file "/media/tomb/org/agenda/agenda.org")
(setq org-capture-templates
      '(

          ("t" "Todo" entry (file "/media/tomb/org/agenda/todos.org")
         (file "~/.emacs.d/etc/templates/todo-template.txt"))

          ("p" "Plan" entry (file "/media/tomb/org/agenda/plans.org")
           (file "~/.emacs.d/etc/templates/plan-template.txt"))

          ("r" "Review" entry (file "/media/tomb/org/agenda/reviews.org")
           (file "~/.emacs.d/etc/templates/review-template.txt"))

          ("b" "Basura" entry (file "~/dc/trash.org")
           (file "~/.emacs.d/etc/templates/basura-template.txt"))

          ("i" "Italiano" entry (file "/media/tomb/org/drill/italiano.org")
           (file "~/.emacs.d/etc/templates/italiano-template.txt"))

          ))

(setq org-refile-targets '((org-agenda-files :maxlevel . 2)))

(use-package org-bullets
  :ensure t
  :config)

(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(use-package evil
  :ensure t
  :init      ;; tweak evil's configuration before loading it
  (setq evil-want-integration t  ;; This is optional since it's already set to t by default.
        evil-want-keybinding nil
        evil-vsplit-window-right t
        evil-split-window-below t
        evil-undo-system 'undo-redo)  ;; Adds vim-like C-r redo functionality
  (evil-mode))

;; (global-set-key (kbd "<left>")
;;   (lambda() 
;;     (interactive)
;;     (message "Use Vim keys: h for Left")))

;; (global-set-key (kbd "<right>")
;;   (lambda() 
;;     (interactive)
;;     (message "Use Vim keys: l for Right")))

;; (global-set-key (kbd "<up>")
;;   (lambda() 
;;     (interactive)
;;     (message "Use Vim keys: k for Up")))

;; (global-set-key (kbd "<down>")
;;   (lambda() 
;;     (interactive)
;;     (message "Use Vim keys: j for Down")))

(use-package evil-collection
  :ensure t
  :after evil
  :config
  (add-to-list 'evil-collection-mode-list 'help) ;; evilify help mode
  (evil-collection-init))
;; Using RETURN to follow links in Org/Evil
(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil))

(use-package diminish
  :ensure t)

(use-package dired-open
  :ensure t
  :after dired
  :config
  (setq dired-open-extensions '(
                                ;; ("jpg" . "nsxiv")
                                ;; ("png" . "nsxiv")
                                ("svg" . "inkscape")
                                ("mp3" . "mpv")
                                ("ogg" . "mpv")
                                ("pdf" . "zathura")
                                ("mkv" . "mpv")
                                ("gif" . "mpv")
                                ("webm" . "mpv")
                                ("mp4" . "mpv"))))

(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode)
            (dired-sort-toggle-or-edit)))

;; Sort directories alphabeticly
(setq dired-listing-switches "-al --dired --group-directories-first -h -G")

(use-package dired-sidebar
  :ensure t
  :commands (dired-sidebar-toggle-sidebar))

(use-package vertico
  :bind (:map vertico-map
         ("C-j" . vertico-next)
         ("C-k" . vertico-previous)
         ("C-f" . vertico-exit)
         :map minibuffer-local-map
         ("M-h" . dw/minibuffer-backward-kill))
  :custom
  (vertico-cycle t)
  :custom-face
  ;(vertico-current ((t (:background "#3a3f5a"))))
  :init
  (vertico-mode))

(use-package savehist
  :ensure nil
  :config
    (setq history-length 25)
    (savehist-mode 1))

(use-package orderless
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles . (partial-completion))))))

(defun dw/minibuffer-backward-kill (arg)

  "When minibuffer is completing a file name delete up to parent
folder, otherwise delete a word"
  (interactive "p")
  (if minibuffer-completing-file-name
      (if (string-match-p "/." (minibuffer-contents))
          (zap-up-to-char (- arg) ?/)
        (delete-minibuffer-contents))
      (backward-kill-word arg)))

(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings in `mode-specific-map'
         ("C-c M-x" . consult-mode-command)
         ([remap Info-search] . consult-info)

         ;; C-x bindings in `ctl-x-map'
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer

         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop

         ;; M-g bindings in `goto-map'
         ;("M-g m" . consult-mark)
         ;("M-g i" . consult-imenu)
         ("M-g g" . consult-goto-line)             ;; orig. goto-line

         ;; M-s bindings in `search-map'
         ;("M-s g" . consult-git-grep)
         ("M-s f" . consult-find)                  ;; Alternative: consult-fd
         ("M-s r" . consult-ripgrep)
         ("M-s u" . consult-focus-lines))

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

)

(add-hook 'text-mode-hook 'abbrev-mode)
(add-hook 'prog-mode-hook 'abbrev-mode)

;(define-abbrev global-abbrev-table "" "")

(use-package hydra
  :ensure t)

(defhydra hydra-navigation ()
  "navigating hydra"
  ("s" (find-file "~/.config/sway/config") "sway")
  ("z" (find-file "~/.zshrc") "zsh")
  ("e" (find-file "~/.emacs.d/config.org") "emacs"))

(use-package corfu
  :ensure t
  ;; Optional customizations
  :custom
  (corfu-cycle t)                 ; Allows cycling through candidates
  (corfu-auto t)                  ; Enable auto completion
  (corfu-auto-prefix 1)
  (corfu-auto-delay 0.4)
  (corfu-popupinfo-delay '(0.5 . 0.5))
  (corfu-preview-current 'insert) ; insert previewed candidate
  (corfu-preselect 'prompt)
  (corfu-on-exact-match nil)      ; Don't auto expand tempel snippets
  ;; Optionally use TAB for cycling, default is `corfu-complete'.
  :bind (:map corfu-map
              ("M-SPC"      . corfu-insert-separator)
              ("TAB"        . corfu-next)
              ([tab]        . corfu-next)
              ("S-TAB"      . corfu-previous)
              ([backtab]    . corfu-previous)
              ;("S-<return>" . corfu-insert)
              ("RET"        . corfu-insert))

  :init
  (global-corfu-mode)
  (corfu-history-mode)
  (corfu-popupinfo-mode) ; Popup completion info
  :config
  (add-hook 'eshell-mode-hook
            (lambda () (setq-local corfu-quit-at-boundary t
                                   corfu-quit-no-match t
                                   corfu-auto nil)
              (corfu-mode))
            nil
            t))

;; Use Dabbrev with Corfu!
(use-package dabbrev
  ;; Swap M-/ and C-M-/
  :bind (("M-/" . dabbrev-completion)
         ("C-M-/" . dabbrev-expand))
  :config
  (add-to-list 'dabbrev-ignored-buffer-regexps "\\` ")
  ;; Since 29.1, use `dabbrev-ignored-buffer-regexps' on older.
  (add-to-list 'dabbrev-ignored-buffer-modes 'doc-view-mode)
  (add-to-list 'dabbrev-ignored-buffer-modes 'pdf-view-mode))

(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p /" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-elisp-symbol)
         ("C-c p e" . cape-elisp-block)
         ("C-c p a" . cape-abbrev)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p :" . cape-emoji)
         ("C-c p t" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  ;;(add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
)

(use-package ledger-mode
   :ensure t)

(add-to-list 'load-path "~/.emacs.d/config/ledger-mode")
(require 'ledger-mode)

(use-package flycheck-ledger
  :ensure t
  :after ledger-mode)

(use-package which-key
  :ensure t
  :init
  (which-key-mode 1)
  :diminish
  :config
  (setq which-key-side-window-location 'bottom
        which-key-sort-order #'which-key-key-order-alpha
        which-key-allow-imprecise-window-fit nil
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10
        which-key-side-window-max-height 0.25
        which-key-idle-delay 0.8
        which-key-max-description-length 25
        which-key-allow-imprecise-window-fit nil
        which-key-separator " > " ))

(setq diary-file "/media/tomb/org/agenda/diary")

(setq calendar-view-diary-initially-flag t
      diary-number-of-entries 7
      diary-display-function #'diary-fancy-display)
(add-hook 'calendar-today-visible-hook 'calendar-mark-today)

(setq org-agenda-include-diary t)

(setq-default abbrev-mode 1)

(use-package yasnippet
  :ensure t
  :defer 2
  :config
  :init
  (yas-global-mode 1))
(add-hook 'emacs-startup-hook (lambda () (yas-load-directory "~/.emacs.d/snippets")))
(yas-reload-all)

(use-package yasnippet-snippets
  :ensure t
  :defer)

(setq ispell-dictionary "es")
(setq ispell-personal-dictionary "~/.emacs.d/var/dictionary.org")

(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(use-package page-break-lines
  :ensure t)
  ;; :defer t
  ;; :diminish ""
  ;; :config
  ;; (set-fontset-font "fontset-default"
  ;;                   (cons page-break-lines-char page-break-lines-char)
  ;;                   (face-attribute 'default :family)))

(setq page-break-lines-mode t)

(use-package highlight-indent-guides
  :config
    (setq highlight-indent-guides-method 'character)
    (setq highlight-indent-guides-auto-enabled nil)

    (set-face-background 'highlight-indent-guides-odd-face "darkgray")
    (set-face-background 'highlight-indent-guides-even-face "dimgray")
    (set-face-foreground 'highlight-indent-guides-character-face "#458588")
    :init (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))

(use-package all-the-icons
  :ensure t
  :if(display-graphic-p))

(use-package nerd-icons
  :ensure t)

(use-package all-the-icons-completion
  :ensure t
  ;(:host github :branch "master" :repo "MintSoup/all-the-icons-completion")
  :config
  (all-the-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup))

(use-package all-the-icons-dired
  :hook (dired-mode . (lambda () (all-the-icons-dired-mode t))))

(use-package rainbow-mode)

(add-hook 'text-mode-hook (lambda () (rainbow-mode t)))
(add-hook 'prog-mode-hook (lambda () (rainbow-mode t)))

(use-package rainbow-delimiters
  :hook ((emacs-lisp-mode . rainbow-delimiters-mode)
         (clojure-mode . rainbow-delimiters-mode)))

(use-package marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-annotators '(marginalia-annonators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package olivetti)

(use-package flycheck
  :ensure t
  :defer t
  :diminish
  :init (global-flycheck-mode))

(use-package hl-todo
  :ensure t
  :hook ((org-mode . hl-todo-mode)
         (prog-mode . hl-todo-mode))
  :config
  (setq hl-todo-highlight-punctuation ":"
        hl-todo-keyword-faces
        `(("TODO"       warning bold)
          ("FIXME"      error bold)
          ("HACK"       font-lock-constant-face italic)
          ("REVIEW"     font-lock-keyword-face bold)
          ("NOTE"       success bold)
          ("DEPRECATED" font-lock-doc-face bold))))

(use-package fireplace
  :ensure t
  :config)

;; (use-package org-drill
;;   :config (progn
;;             (add-to-list 'org-modules 'org-drill)
;;             (setq org-drill-add-random-noise-to-intervals-p t)
;;             (setq org-drill-hint-separator "||")
;;             (setq org-drill-left-cloze-delimiter "<[")
;;             (setq org-drill-right-cloze-delimiter "]>")
;;             (setq org-drill-learn-fraction 0.25)
;;             (setq org-drill-use-visible-cloze-face-p t) ;;cloze-deleted text show special font
;;             (setq org-drill-hide-item-headings-p t) ;; item headings made invisible while each item is being tested
;;             (setq org-drill-maximum-items-per-session 40) ;; number of review items
;;              ;(setq org-drill-save-buffers-after-drill-sessions-p nil) ;; prompted to save all unsaved buffers at the end of a drill session
;;             (setq org-drill-maximum-duration 10) ;; minutes
;;             (setq org-drill-overdue-interval-factor 1.1) ;; cannot be less than 1.0
;;             (setq org-drill-days-before-old 7) ;; inter-repetition interval of 10 days or less
;;             (setq org-drill-adjust-intervals-for-early-and-late-repetitions-p t)
;;             (setq org-drill-learn-fraction 0.45)   ; change the value as desired
;;             (setq org-drill-directory "/media/tomb/org/drill")))

;; (use-package emms
;;     :commands emms
;;     :bind (("C-c m n" . emms-next)
;;            ("C-c m p" . emms-previous)
;;            ("C-c m b" . emms-smart-browse)
;;            ("C-c m l" . emms-playlist-mode-go)
;;            ("C-c m SPC" . emms-pause)
;;            ("C-c m a" . emms-add-directory-tree)
;;            ("C-c m s s" . emms-browser-search-by-names)
;;            ("C-c m u" . emms-player-mpd-update-all-reset-cache))
;;     :config
;;     (require 'emms-setup)
;;     (require 'emms-player-mpd)
;;     (emms-standard)
;;     (emms-default-players)
;;     (setq emms-mode-line-format "♫ %s ")
;;     (setq emms-player-list '(emms-player-mpd))
;;     (setq emms-player-mpd-server-name "localhost")
;;     (setq emms-player-mpd-server-port "6600"))

;; (use-package denote
;;   :ensure t
;;   :config
;;   ;;
;;   ;; General key bindings
;;   (setq denote-directory "~/dc/notas/")
;;   (setq denote-known-keywords '("trabajo" "estudio"))
;;   (setq denote-infer-keywords t)
;;   (setq denote-sort-keywords t)
;;   ;;
;;   ;; Tweaking the frontmatter
;;  (setq denote-org-front-matter
;;         "#+title: %s\n#+author: toru\n#+date: %s\n#+filetags: %s\n#+identifier: %s\n\n")
;;   :bind
;;   ("C-c n n" . denote-open-or-create)
;;   ("C-c n l" . denote-link-or-create)
;;   ("C-c n B" . denote-link-find-file)
;;   ("C-c n b" . denote-link-backlinks))

;; (use-package ace-window
;;   :ensure t
;;   :config)

;; (setq avy-timeout 0.5) ; set duration for the variable

;; (use-package embark
;;   :ensure t
;;   :bind
;;   (("C-." . embark-act)         ;; pick some comfortable binding
;;    ("C-;" . embark-dwim)        ;; good alternative: M-.
;;    ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

;;   :init

;;   ;; Optionally replace the key help with a completing-read interface
;;   (setq prefix-help-command #'embark-prefix-help-command)

;;   ;; Show the Embark target at point via Eldoc. You may adjust the
;;   ;; Eldoc strategy, if you want to see the documentation from
;;   ;; multiple providers. Beware that using this can be a little
;;   ;; jarring since the message shown in the minibuffer can be more
;;   ;; than one line, causing the modeline to move up and down:

;;   ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
;;   ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

;;   :config

;;   ;; Hide the mode line of the Embark live/completions buffers
;;   (add-to-list 'display-buffer-alist
;;                '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
;;                  nil
;;                  (window-parameters (mode-line-format . none)))))

;; ;; Consult users will also want the embark-consult package.
;; (use-package embark-consult
;;   :ensure t ; only need to install it, embark loads it after consult if found
;;   :hook
;;   (embark-collect-mode . consult-preview-at-point-mode))

;; ;(add-to-list 'load-path "/usr/bin/mu/mu4e")
;; (require 'mu4e)
;; (require 'smtpmail)
;; (setq message-send-mail-function 'smtpmail-send-it
;;       smtpmail-auth-credentials "~/.authinfo")
;; (setq mu4e-attachment-dir "~/dl")
;; (setq mu4e-compose-complete-addresses t)
;; (setq mu4e-compose-context-policy 'always-ask)
;; (setq mu4e-compose-dont-reply-to-self t)
;; (setq mu4e-context-policy 'pick-first)
;; ;(setq mu4e-get-mail-command "~/scripts/mail_sync.sh")
;; (setq mu4e-headers-date-format "%d-%m-%Y %H:%M")
;; (setq mu4e-headers-fields '((:human-date . 20)
;;                             (:flags . 6)
;;                             (:mailing-list . 10)
;;                             (:from . 22)
;;                             (:subject)))
;; (setq mu4e-headers-include-related t)
;; (setq mu4e-sent-messages-behavior 'delete)
;; (setq mu4e-view-show-addresses t)
;; (setq mu4e-view-show-images t)
;; (setq smtpmail-debug-info t)

;; ;; mu4e context
;; (setq mu4e-contexts
;;       `(

;;  ;; ,(make-mu4e-context
;;  ;;    :name "cock"
;;  ;;    :match-func (lambda (msg)
;;  ;;    (when msg
;;  ;;      (mu4e-message-contact-field-matches msg
;;  ;;                            :to "usercount_1@disroot.org")))
;;  ;;    :vars '(
;;  ;;     (smtpmail-smtp-server . "disroot.org")
;;  ;;     (smtpmail-smtp-service . 587)
;;  ;;     (smtpmail-stream-type . starttls)
;;  ;;     (smtpmail-local-domain . "disroot.org")
;;  ;;     (user-mail-address . "usercount_1@disroot.org")
;;  ;;     (mu4e-sent-folder . "/disroot/Sent")
;;  ;;     (mu4e-drafts-folder . "/disroot/Drafts")
;;  ;;     (mu4e-trash-folder . "/disroot/Trash")
;;  ;;     (mu4e-maildir-shortcuts
;;  ;;      . ((:maildir "/disroot/INBOX" :key ?i)
;;  ;;         (:maildir "/disroot/Sent" :key ?s)
;;  ;;         (:maildir "/disroot/Drafts" :key ?d)
;;  ;;         (:maildir "/disroot/Trash" :key ?t)))))

;;   ,(make-mu4e-context
;;     :name "cock"
;;     :match-func (lambda (msg)
;;     (when msg
;;       (mu4e-message-contact-field-matches msg
;;                             :to "toru777average@cock.li")))
;;     :vars '(
;;      (smtpmail-smtp-server . "mail.cock.li")
;;      (smtpmail-smtp-service . 587)
;;      (smtpmail-stream-type . starttls)
;;      (smtpmail-local-domain . "mail.cock.li")
;;      (user-mail-address . "toru777average@cock.li")
;;      (mu4e-sent-folder . "/cock/Sent")
;;      (mu4e-drafts-folder . "/cock/Drafts")
;;      (mu4e-trash-folder . "/cock/Trash")
;;      (mu4e-maildir-shortcuts
;;       . ((:maildir "/cock/INBOX" :key ?i)
;;          (:maildir "/cock/Sent" :key ?s)
;;          (:maildir "/cock/Drafts" :key ?d)
;;          (:maildir "/cock/Trash" :key ?t)))))))

;(setq newsticker-frontend 'newsticker-plainview)

;; (use-package perspective
;;   :ensure t
;;   :custom
;;   ;; NOTE! I have also set 'SCP =' to open the perspective menu.
;;   ;; I'm only setting the additional binding because setting it
;;   ;; helps suppress an annoying warning message.
;;   (persp-mode-prefix-key (kbd "C-c M-p"))
;;   :init
;;   (persp-mode)
;;   :config
;;   ;; Sets a file to write to when we save states
;;   (setq persp-state-default-file "~/.emacs.d/perspectives"))

;; ;; This will group buffers by persp-name in ibuffer.
;; (add-hook 'ibuffer-hook
;;           (lambda ()
;;             (persp-ibuffer-set-filter-groups)
;;             (unless (eq ibuffer-sorting-mode 'alphabetic)
;;               (ibuffer-do-sort-by-alphabetic))))

;; ;; Automatically save perspective states to file when Emacs exits.
;; ;(add-hook 'kill-emacs-hook #'persp-state-save)

;; (use-package undo-tree)

;; ; Not history
;; ;(setq undo-tree-auto-save-history nil)

;; ;; Prevent undo tree files from polluting your computer
;; (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/auto-save-list")))

;; (setq undo-tree-visualizer-diff t)
